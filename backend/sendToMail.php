<?php
session_start();
include('../settings.php');

if(!isset($_SESSION['mailSend']))
{
    //grecaptcha: response, data_fistname: data_fistname, data_lastname: data_fistname, data_email: data_email, data_text: data_text
    if(isset($_POST['grecaptcha']) && isset($_POST['data_fistname']) && isset($_POST['data_lastname']) && isset($_POST['data_email']) && isset($_POST['data_text']))
    {
        $grecaptcha_response = $_POST['grecaptcha'];
        $data_firstname = $_POST['data_fistname'];
        $data_lastname = $_POST['data_lastname'];
        $data_email = $_POST['data_email'];
        $data_text = $_POST['data_text'];

        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$sec_recapcha_key."&response=".$grecaptcha_response."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false)
        {
            echo 'Validation failed!';
            return;
        }
        else
        {
            if($data_firstname == "" || $data_lastname == "" || strlen($data_firstname) > 50 || strlen($data_lastname) > 50)
            {
                echo 'Name not given or too long!';
                return;
            }

            if($data_email == "" || strlen($data_email) > 200)
            {
                echo 'EMail not given!';
                return;
            }

            if($data_text == "" || strlen($data_text) > 5000)
            {
                echo 'Text not given or too long!';
                return;
            }

            $to      = $email;
            $subject = 'DR.COM | MSG';
            $message = $data_firstname . ' ' . $data_lastname . ': ' . $data_text;
            $headers = 'From: ' . $data_email . "\r\n" .
                'Reply-To: ' . $data_email . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $message, $headers);

            $_SESSION['mailSend'] = true;
            echo 'success';
            return;
        }
    }
}
else
{
  echo 'It seems like you already send me a request!';
  return;
}
