<?php
$txt_developer = "Entwickler";
$txt_software = "Software";
$txt_quote_1 = "Neuste Webstandarts, wunderschönes Design!";
$txt_quote_2 = "Er ist wirklich einer der fähigsten Programmierer.";
$txt_quote_3 = "Das Endergebnis war überwältigend";
$txt_quote_4 = "Viele Fähigkeiten in vielen Feldern der Programmierung!";
$txt_quote_5 = "Er hat wirklich jede Menge 'know how'.";
$txt_my_work_perfect_for = "Meine Arbeit ist perfekt für..";
$txt_read_more = "Mehr";
$txt_contact_me = "Kontaktieren"; //Sie mich | fixed due to size collisions
$txt_contact_me_via_email = "Kontaktieren Sie mich via E-Mail";
$txt_projects = "Projekte";
$txt_repository_of_this_website = "Repository dieser Webseite";
$txt_imprint = "Impressum";
$txt_owner = "Besitzer";
$txt_contact = "Kontakt";
$txt_phone = "Telefon";
$txt_adress = "Wohnsitz";
$txt_view_the_project = "Das Projekt online anschauen";
$txt_download_the_project = "Das Projekt herunterladen";
$txt_view_the_rep = "Das Repository anschauen";
$txt_startups = "Startups";
$txt_businesses = "Unternehmen";
$txt_your_creative_ideas = "Ihre kreativen Ideen";
$txt_so_whats_your_name = "Also, wie lautet Ihr Name?";
$txt_next = "Weiter";
$txt_how_can_i_contact_you = "Wie kann ich Sie kontaktieren?";
$txt_reason_to_contact_me = "Was ist Ihr Grund mich zu kontaktieren?";
$txt_pls_solve_this = "Bitte lösen sie nun Folgendes";
$txt_thx_for_your_msg = "Vielen Dank für Ihre Nachricht!";
$txt_step = "Schritt";
$txt_first_name = "Vorname";
$txt_last_name = "Nachname";
$txt_your_idea = "Ihre Idee..";
$txt_pls_fill_both = "Bitte beide Felder ausfüllen!";
$txt_firstname_too_long = "Vorname zu lang oder zu kurz!";
$txt_lastname_too_long = "Nachname zu lang oder zu kurz!";
$txt_error = "Fehler";
$txt_pls_fill_the_field = "Bitte das Feld ausfüllen!";
$txt_email_too_long = "E-Mail zu lang oder zu kurz!";
$txt_email_invalid = "Diese E-Mail ist ungültig!";
$txt_text_has_min = "Text hat ein Minimum von 20 und Maximum von 500 Zeichen.";
$txt_capchat_not_veryf = "Captcha wurde nicht korrekt gelöst";
$txt_sending = "Sende..";
$txt_change_lang_ger = "Sprache zu Deutsch ändern";
$txt_change_lang_eng = "Sprache zu Englisch ändern";
$txt_comp_that_trust_me = "Firmen";
$txt_hey_my_name_is = "Hey, mein Name ist";
$txt_self_desc = "<ul>".
    "<li>19 Jahre alter <span class='bold'>Software Entwickler</span> hier aus <span class='bold'>Siegen (NRW)</span></li>".
    "<li>Ich liebe das Programmieren! In verschienenen IT Branchen u.a. <span class='fancy'>Web Development, Game Development and Business IT Solutions</span> habe ich bereits Erfahrungen gesammelt.</li>".
    "<li>Ich habe <span class='bold'>3 Jahre Praxiserfahrung als Software Entwickler</span> bei NEO 7EVEN GmbH und ich suche immer nach neuen Herausforderungen.</li>".
"</ul>".

"<div class='cutline'></div>".

"Also, <b>was</b> biete ich <b>deinem Unternehmen</b>?".
"<ul>".
    "<li>Erfarung in vielen Programmiersprachen u.a.: <br><b>Java, PHP, JavaScript, HTML5, CSS3, ActionScript</b></li>".
    "<li>Und den dazugehörigen Erweiterungen: <br><b>Node.js, JQuery, Socket.io, SASS, LESS, FLEX</b></li>".
    "<li>Lösungen basierend auf den neusten Standarts</li>".
    "<li>Sauberen Code</li>".
    "<li>Professionalität</li>".
    "<li>Fachwissen</li>".
"</ul>";
