<?php
$txt_developer = "Developer";
$txt_software = "Software";
$txt_quote_1 = "Newest webstandarts, beautiful design!";
$txt_quote_2 = "This guy has skills..";
$txt_quote_3 = "The result was overwhelming.";
$txt_quote_4 = "I realy like his range of abilitys - this guy has 'know how'.";
$txt_quote_5 = "A very skilled and young developer!";
$txt_my_work_perfect_for = "My work is perfect for..";
$txt_read_more = "Read More";
$txt_contact_me = "Contact Me";
$txt_contact_me_via_email = "Contact me via E-Mail";
$txt_projects = "Projects";
$txt_repository_of_this_website = "Repository Of This Website";
$txt_imprint = "Imprint";
$txt_owner = "Owner";
$txt_contact = "Contact";
$txt_phone = "Phone";
$txt_adress = "Address";
$txt_view_the_project = "View the project";
$txt_download_the_project = "Download the project";
$txt_view_the_rep = "View the repository";
$txt_startups = "Startups";
$txt_businesses = "Businesses";
$txt_your_creative_ideas = "Your creative ideas";
$txt_so_whats_your_name = "So, what's your name?";
$txt_next = "Next";
$txt_how_can_i_contact_you = "How can I contact you?";
$txt_reason_to_contact_me = "What's your reason to contact me?";
$txt_pls_solve_this = "Now please solve this";
$txt_thx_for_your_msg = "Thanks for your message!";
$txt_step = "Step";
$txt_first_name = "Firstname";
$txt_last_name = "Lastname";
$txt_your_idea = "Your idea..";
$txt_pls_fill_both = "Please fill both fields!";
$txt_firstname_too_long = "Firstname to long or to short!";
$txt_lastname_too_long = "Lastname to long or to short!";
$txt_error = "Error";
$txt_pls_fill_the_field = "Please fill the field!";
$txt_email_too_long = "E-Mail to long or to short!";
$txt_email_invalid = "This E-Mail is invalid!";
$txt_text_has_min = "Text has a min. 20 and max. 500 characters.";
$txt_capchat_not_veryf = "Captcha is not verifyed";
$txt_sending = "Sending..";
$txt_change_lang_ger = "Change language to german";
$txt_change_lang_eng = "Change language to english";
$txt_hey_my_name_is = "Hey, my name is";
$txt_self_desc = "<ul>".
    "<li>19 year old <span class='bold'>software developer</span> from <span class='bold'>Germany</span></li>".
    "<li>I have a addiction to programming in many different technologie subjects of the IT industry such as <span class='fancy'>Web Development, Game Development and Business IT Solutions</span></li>".
    "<li>I have <span class='bold'>3 years of job experience as software developer</span> at NEO 7EVEN and I always search for new challenges.</li>".
"</ul>".

"<div class='cutline'></div>".

"So now, <b>what</b> do <b>I offer to you</b>?".
"<ul>".
    "<li>Experience in many programming languages such as: <br><b>Java, PHP, JavaScript, HTML5, CSS3, ActionScript</b></li>".
    "<li>And corresponding extensions: <br><b>Node.js, JQuery, Socket.io, SASS, LESS, FLEX</b></li>".
    "<li>Solutions based on newest standarts</li>".
    "<li>Clean Coding</li>".
    "<li>Professionality</li>".
    "<li>Personality</li>".
"</ul>";
