window.onload = function()
{
    //Enable tooltips
    $('[data-toggle="tooltip"]').tooltip();

    $('#stepCounter').fadeOut(0);
    $('#workScope_error').fadeOut(0);

    var el = document.querySelector('input');

    if(el && el != null)
    {
      el.addEventListener('keydown', function (e) {
          if (e.which == 9) {
              e.preventDefault();
          }
      });
    }

    $("#subShadowContent").addClass('isShowing');
    $("#picture").addClass('isShowing');

    changeQuote();
    setInterval(changeQuote, 5000);

    $('#socialMedia').addClass('isShowing');

    setTimeout(function(){
        $('#jobDesc').addClass('isShowing');

        $('#jobDesc .word').each(function(i){

            setTimeout(function()
            {
              $('#jobDesc .word').eq(i).addClass('isShowing');
            }, 1000);// * (i + 1));

        });

    }, 1800);

    setTimeout(function(){
        $('fadeOut').fadeOut(100);
    }, 1000);

    setTimeout(function(){

        $('#socialMedia ul a').each(function(i){

            setTimeout(function()
            {
              $('#socialMedia ul a').eq(i).addClass('isShowing');
            }, 500 * (i + 1));

        });

    }, 3000);

    $('.moreInfoBtn').click(function(){
        $('.pinched').addClass('isShowing');
        $('.moreInfoBtn').addClass('activated');
    });

    $(document).click(function(event) {
        if(!$(event.target).closest('.pinched').length &&
           !$(event.target).is('.pinched') && !$(event.target).is('.moreInfoBtn')) {
             $('.pinched').removeClass('isShowing');
             $('.moreInfoBtn').removeClass('activated');
        }
    });
}

var cur = 0;

function changeQuote()
{
    cur++;
    if(cur > quotes.length-1)
    {
        cur = 0;
    }

    $("#ideaLine li").fadeOut(500, function(){
        $("#ideaLine li").html(quotes[cur]);
        $("#ideaLine li").fadeIn(500);
    });
}

function scrollTo(id)
{
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top - 100},
    'slow');
}

$(window).scroll(function(){

    var scrollY = $(this).scrollTop();

    //Fade in skills
    /*if(scrollY > $('#skills').offset().top - ($(window).height() / 3))
    {
        $('#skills .skillItem').each(function(i){

            setTimeout(function()
            {
              $('#skills .skillItem').eq(i).addClass('isShowing');
            }, 250 * (i + 1));

        });

    }*/

    if(scrollY > $('#contact .contactMeBtn').offset().top - ($(window).height() / 1.8))
    {
        $('#contact .contactMeBtn').addClass('isShowing');
    }

    if(scrollY > $('#aboutMe .skillset').offset().top - ($(window).height() / 1.5))
    {
         $('#aboutMe .skillset .skill').each(function(i){

            setTimeout(function()
            {
              $('#aboutMe .skillset .skill').eq(i).addClass('isShowing');
            }, 200 * (i + 1));

        });
    }
});

var data_fistname, data_lastname, data_email, data_text;

function check_workScope_1()
{

    $('#stepCounter').fadeIn(2000);
    $('#workScope_step1').removeClass('isShowing');
    $('#workScope_step2').addClass('isShowing');
}

function check_workScope_2() //Name
{
    data_fistname = $('#firstnameInput').val();
    data_lastname = $('#lastnameInput').val();

    if(!data_fistname || !data_lastname || data_fistname == "" || data_lastname == "")
    {
        show_workScope_error(txt_pls_fill_both);
        return;
    }

    if(data_fistname.length < 3 || data_fistname.length > 20)
    {
        show_workScope_error(txt_firstname_too_long);
        return;
    }

    if(data_lastname.length < 3 || data_lastname.length > 40)
    {
        show_workScope_error(txt_lastname_too_long);
        return;
    }

    $('#stepCounter .cText').html('2 / 4');
    $('#workScope_step2').addClass('isHidden');
    $('#workScope_step3').addClass('isShowing');
}

function check_workScope_3() //Email
{
    data_email = $('#emailInput').val();

    if(!data_email || data_email == "")
    {
        show_workScope_error(txt_pls_fill_the_field);
        return;
    }

    if(data_email.length < 6 || data_email.length > 100)
    {
        show_workScope_error(txt_email_too_long);
        return;
    }

    if(!validateEmail(data_email))
    {
        show_workScope_error(txt_email_invalid);
        return;
    }

    $('#stepCounter .cText').html('3 / 4');
    $('#workScope_step3').addClass('isHidden');
    $('#workScope_step4').addClass('isShowing');
}

function check_workScope_4() //Text
{
    data_text = $('#textInput').val();

    if(!data_text || data_text == "")
    {
        show_workScope_error(txt_pls_fill_the_field);
        return;
    }

    if(data_text.length < 20 || data_text.length > 500)
    {
        show_workScope_error(txt_text_has_min);
        return;
    }

    $('#stepCounter .cText').html('4 / 4');
    $('#workScope_step4').addClass('isHidden');
    $('#workScope_step5').addClass('isShowing');
}

function check_workScope_5() //Captcha
{
    var response = grecaptcha.getResponse();

    if(response.length == 0)
    {
        show_workScope_error(txt_capchat_not_veryf);
        return;
    }


    $('#stepCounter').fadeOut(500);
    $('#workScope_step5').addClass('isHidden');
    $('#workScope_step6').addClass('isShowing');
    $('#workScope_step6 li').html(txt_sending);

    //SEND MSG TO BACKEND
    $.post("backend/sendToMail.php", {grecaptcha: response, data_fistname: data_fistname, data_lastname: data_fistname, data_email: data_email, data_text: data_text}, function(data){
        if(data == "success")
        {
            $('#workScope_step6 li').html(txt_thx_for_your_msg);
            $('#workScope').addClass('isHalfClosed');

            setTimeout(function(){
                $('#workScope').removeClass('isHalfClosed');
                $('#workScope').addClass('isClosed');
            }, 2500);
        }
        else
        {
            $('#workScope_step6 li').html(data);
        }
    });
}

function show_workScope_error(msg)
{
    $('#workScope_error p').html(msg);
    $('#workScope_error').fadeIn(500);
}

/* GOOGLE ANALYTICS */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-80421964-1', 'auto');
ga('send', 'pageview');

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
