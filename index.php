<?php
    session_start();
    include('settings.php');

    $lang = "en";

    if(isset($_GET['lang']) && $_GET['lang']=='de')
    {
      echo "A";
      $lang = "de";
    }

    include('lang.php');

    if(isset($_GET['a']))
    {
      include('/backend/analysis.php');
    }
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8"/>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- SEO -->
        <meta name="description" content="<?php echo $firstname; ?> <?php echo $lastname; ?> - A young developer." />
        <meta name="keywords" content="<?php echo $firstname; ?> <?php echo $lastname; ?> Developer Young Coding Freelancer Talent Webdeveloper Website Gamedeveloper" />
        <meta name="author" content="meta-tags generator">
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="2 days" />

        <meta itemprop="name" content="<?php echo $firstname; ?> <?php echo $lastname; ?>">
        <meta itemprop="description" content="A young developer.">
        <meta itemprop="image" content="http://dennisrauscher.de/gfx/PB154-comp.jpg">

        <meta name="twitter:card" content="<?php echo $firstname; ?> <?php echo $lastname; ?>">
        <meta name="twitter:site" content="">
        <meta name="twitter:title" content="<?php echo $firstname; ?> <?php echo $lastname; ?>">
        <meta name="twitter:description" content="A young developer.">
        <meta name="twitter:creator" content="">
        <meta name="twitter:image:src" content="http://dennisrauscher.de/gfx/PB154-comp.jpg">
        <meta name="twitter:player" content="">

        <meta property="og:url" content="http://dennisrauscher.de">
        <meta property="og:title" content="<?php echo $firstname; ?> <?php echo $lastname; ?>">
        <meta property="og:description" content="A young developer.">
        <meta property="og:site_name" content="<?php echo $firstname; ?> <?php echo $lastname; ?>">
        <meta property="og:type" content="article">
        <meta property="og:locale" content="de_DE">

        <title><?php echo $firstname; ?> <?php echo $lastname; ?></title>

        <link rel="icon" type="image/png" href="/gfx/PB154.jpg"/>

        <link rel="stylesheet" href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="public/stylesheets/index.css"/>

        <script>
        var quotes = [
          "„<?php echo $txt_quote_1; ?>“",
          "„<?php echo $txt_quote_2; ?>“",
          "„<?php echo $txt_quote_3; ?>“",
          "„<?php echo $txt_quote_4; ?>“",
          "„<?php echo $txt_quote_5; ?>“"
        ];
          /* Transaltion variables */
          var txt_pls_fill_both = "<?php echo $txt_pls_fill_both; ?>";
          var txt_firstname_too_long = "<?php echo $txt_firstname_too_long; ?>";
          var txt_lastname_too_long = "<?php echo $txt_lastname_too_long; ?>";
          var txt_pls_fill_the_field = "<?php echo $txt_pls_fill_the_field; ?>";
          var txt_email_too_long = "<?php echo $txt_email_too_long; ?>";
          var txt_email_invalid = "<?php echo $txt_email_invalid; ?>";
          var txt_text_has_min = "<?php echo $txt_text_has_min; ?>";
          var txt_capchat_not_veryf = "<?php echo $txt_capchat_not_veryf; ?>";
          var txt_sending = "<?php echo $txt_sending; ?>";
          var txt_thx_for_your_msg = "<?php echo $txt_thx_for_your_msg; ?>";
        </script>

        <?php if(isset($_GET['qr-demo'])){ ?>
          <script id="qr4blindSRC" src="plugin/qr4blind.js" mode="both" lang="<?php echo $lang; ?>"></script>
        <?php } ?>
        <script src="js/jquery-1.12.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <nav>
            <ul>
                <li onclick="scrollTo('picture');"><?php echo $firstname; ?> <?php echo $lastname; ?></li>
                <li onclick="scrollTo('projects');"><?php echo $txt_projects; ?></li>
                <li onclick="scrollTo('contact');"><?php echo $txt_contact; ?></li>
                <li class="short" data-toggle="tooltip" data-placement="bottom" title="<?php echo $txt_change_lang_ger; ?>"><a href="?lang=de">DE</a></li>
                <li class="short" data-toggle="tooltip" data-placement="bottom" title="<?php echo $txt_change_lang_eng; ?>"><a href="?lang=en">EN</a></li>
            </ul>
        </nav>

        <section id="picture">
            <div class="picMe">
                <div class="picLayover">
                    <li><a href="mailto:<?php echo $email; ?>"><?php echo $txt_contact_me; ?></a></li>
                </div>
            </div>
        </section>

        <div id="subShadowContent">
            <section id="tanszone">
                <li><?php echo $firstname; ?></li>
                <li></li>
                <li><?php echo $lastname; ?></li>
            </section>
            <section id="jobDesc">
                <!--<div class="word bottom">Professional</div>-->
                <div class="word bottom"><b><?php echo $txt_software; ?></b></div>
                <!--<div class="word first"><b>[ </b></div>
                <div class="word sec"><b> ]</b></div>-->
                <div class="word"><b><?php echo $txt_developer; ?></b></div>
            </section>
            <section id="socialMedia">
                <ul>
                    <a href="mailto: <?php echo $email; ?>"><i class="fa fa-envelope-o" data-toggle="tooltip" data-placement="top" title="E-Mail"></i></a>
                    <a href="http://de.linkedin.com/in/DennisRauscher" target="_blank"><i class="fa fa-linkedin-square" data-toggle="tooltip" data-placement="top" title="LinkedIn"></i></a>
                    <a href="https://www.xing.com/profile/Dennis_Rauscher4" target="_blank"><i class="fa fa-xing-square"  data-toggle="tooltip" data-placement="top" title="Xing"></i></a>
                </ul>
            </section>

            <section id="ideaLine">
                <li></li>
            </section>
            <!--<section id="skills">
                <div class="wrapper">
                    <div class="headline"><li>Skills</li></div>
                    <div class="cutline"></div>
                    <table>
                        <tr>
                            <td class="skillItem">
                                UI-Design:
                                <div class="right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"><i class="fa fa-star-half-o"></i></i></div>
                            </td>
                            <td></td>
                            <td class="skillItem">
                                Java:
                                <div class="right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="skillItem">
                                JavaScript:
                                <div class="right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></div>
                            </td>
                            <td></td>
                            <td class="skillItem">
                                JQuery:
                                <div class="right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="skillItem">
                                AS3:
                                <div class="right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i></div>
                            </td>
                            <td></td>
                            <td class="skillItem">
                                PHP:
                                <div class="right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></div>
                            </td>
                        </tr>
                    </table>
                    <div id="cutline"></div>
                </div>
            </section>-->
            <?php
            if(!isset($_SESSION['mailSend']))
            {
            ?>
            <section id="workScope">
                <div id="workScope_step1" class="isShowing">
                    <li><i class="fa fa-thumbs-up" aria-hidden="true"></i> <?php echo $txt_my_work_perfect_for; ?></li>
                    <div id="typeWrapper">
                        <div id="type" onclick="check_workScope_1();">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <li><?php echo $txt_startups; ?></li>
                        </div>
                        <div id="type" onclick="check_workScope_1();">
                            <i class="fa fa-trophy" aria-hidden="true"></i>
                            <li><?php echo $txt_businesses; ?></li>
                        </div>
                        <div id="type" onclick="check_workScope_1();">
                            <i class="fa fa-space-shuttle" aria-hidden="true"></i>
                            <li><?php echo $txt_your_creative_ideas; ?></li>
                        </div>
                    </div>
                </div>
                <div id="workScope_step2">
                    <li><?php echo $txt_so_whats_your_name; ?></li>
                    <div class="inputs">
                        <input id="firstnameInput" class="nameInput" type="text" placeholder="<?php echo $txt_first_name; ?>"/> <input id="lastnameInput" class="nameInput" type="text" placeholder="<?php echo $txt_last_name; ?>"/>
                    </div>
                    <div class="nextBtn" onclick="check_workScope_2();"><?php echo $txt_next; ?> <i class="fa fa-check-circle" aria-hidden="true"></i></div>
                </div>
                <div id="workScope_step3">
                    <li><?php echo $txt_how_can_i_contact_you; ?></li>
                    <input id="emailInput" class="nameInput" type="email" placeholder="E-Mail"/>
                    <div class="nextBtn" onclick="check_workScope_3();"><?php echo $txt_next; ?> <i class="fa fa-check-circle" aria-hidden="true"></i></div>
                </div>
                <div id="workScope_step4">
                    <li><?php echo $txt_reason_to_contact_me; ?></li>
                    <textarea id="textInput" class="nameInput" type="textarea" placeholder="<?php echo $txt_your_idea; ?>"></textarea>
                    <div class="nextBtn" onclick="check_workScope_4();"><?php echo $txt_next; ?> <i class="fa fa-check-circle" aria-hidden="true"></i></div>
                </div>
                <div id="workScope_step5">
                    <li><?php echo $txt_pls_solve_this; ?>:</li>

                    <div class="text-xs-center">
                        <div id="captcha" class="g-recaptcha" data-sitekey="6LeXrw0UAAAAALioTSgfGXb_HtrbDDBjo5MemwA-"></div>
                    </div>
                    <div class="nextBtn" onclick="check_workScope_5();"><?php echo $txt_next; ?> <i class="fa fa-check-circle" aria-hidden="true"></i></div>
                </div>
                <div id="workScope_step6">
                    <li><?php echo $txt_thx_for_your_msg; ?></li>
                </div>


                <div id="workScope_error" onclick="$('#workScope_error').fadeOut(500);">
                    <li><i class="fa fa-times-circle" aria-hidden="true"></i> <?php echo $txt_error; ?></li>
                    <p>Error 1 is here!</p>
                </div>

                <div id="stepCounter">
                    <p><?php echo $txt_step; ?></p>
                    <div class="cText">1 / 4</div>
                </div>
            </section>
            <?php
            }
            ?>

            <section id="liveProgramming">
                <!-- Maybe a description of my work -->
                <div class="disclaimer">
                    <li>Clean & Fast</li>
                </div>
            </section>
            <section id="aboutMe">
                <div class="moreInfoBtn"><i class="fa fa-user" aria-hidden="true"></i> <?php echo $txt_read_more; ?></div>
                <div class="pinched">
                     <div class="introTxt" qr-rel>
                        <div class="super"><?php echo $txt_hey_my_name_is; ?> <span class="bold"><?php echo $firstname; ?> <?php echo $lastname; ?></span>!</div>
                        <?php echo $txt_self_desc; ?>
                    </div>
                </div>
                <ul class="skillset" qr-rel>
                    <li class="skill">UI/UX-Design</li>
                    <li class="skill">HTML <i class="fa fa-html5" aria-hidden="true"></i></li>
                    <li class="skill">CSS <i class="fa fa-css3" aria-hidden="true"></i></li>
                    <li class="skill">SASS</li>
                    <li class="skill">JavaScript</li>
                    <li class="skill">JQuery</li>
                    <li class="skill">Angular2</li>
                    <li class="skill">NodeJS</li>
                    <li class="skill">WebSockets</li>
                    <li class="skill">SocketIO</li>
                    <li class="skill">Java</li>
                    <li class="skill">PHP</li>
                    <li class="skill">Laravel</li>
                    <li class="skill">Photoshop</li>
                    <li class="skill">Scrum</li>
                    <li class="skill">Git <i class="fa fa-git-square" aria-hidden="true"></i></li>
                    <li class="skill">Jira</li>
                    <li class="skill">C#</li>
                    <li class="skill">ASP.NET</li>
                    <li class="skill">MSSQL</li>
                    <li class="skill">MySQL</li>
                    <li class="skill">Material Design</li> und Mehr..
                </ul>
            </section>
            <section id="contact">
                <div class="wrapper">
                <li><?php echo $txt_contact_me; ?></li>
                    <div class="contactMeBtn"><a href="mailto:<?php echo $email; ?>" data-toggle="tooltip" data-placement="right" title="<?php echo $txt_contact_me_via_email; ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></div>
                </div>
            </section>
            <section id="projects">
                <div class="wrapper">
                    <div class="headline"><li><?php echo $txt_projects; ?></li></div>
                    <div class="cutline"></div>

                    <div class="projects">
                        <div class="project etics">
                            <div class="caption">ETICS Konfigurator <br/><b>2018</b></div>
                            <div class="optionLine">
                                <a class="viewBtn" href="https://eticsconfigurator.com" target="_blank" data-toggle="tooltip" data-placement="bottom" title="<?php echo $txt_view_the_project; ?>" data-container="body"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="projects">
                        <div class="project stesi">
                            <div class="caption">Stellenangebote Siegen <br/><b>2017-2018</b></div>
                            <div class="optionLine">
                                <a class="viewBtn" href="https://stellenangebote-siegen.de" target="_blank" data-toggle="tooltip" data-placement="bottom" title="<?php echo $txt_view_the_project; ?>" data-container="body"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="projects">
                        <div class="project ehc">
                            <div class="caption">EHC Homepage <br/><b>2015-2018</b></div>
                            <div class="optionLine">
                                <a class="viewBtn" href="http://erndtebrueckerhc.de" target="_blank" data-toggle="tooltip" data-placement="bottom" title="<?php echo $txt_view_the_project; ?>" data-container="body"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                <a class="reposBtn" href="https://bitbucket.org/Dexkill/ehc-homepage" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Repository" data-container="body"><i class="fa fa-bitbucket" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="projects">
                        <div class="project whiteVoid">
                            <div class="caption">White Void <br/><b>2015</b></div>
                            <div class="optionLine">
                                <a class="viewBtn" href="https://bitbucket.org/Dexkill/whitevoid-basegameje-example/downloads/WhiteVoid.jar" target="_blank" data-toggle="tooltip" data-placement="bottom" title="<?php echo $txt_download_the_project; ?>" data-container="body"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                <a class="reposBtn disabled" data-toggle="tooltip" data-placement="bottom" title="Repository" data-container="body"><i class="fa fa-bitbucket" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="companys">
                <div class="wrapper">
                <li><?php echo $txt_comp_that_trust_me; ?></li>
                    <div class="companyList">
                        <div class="company company_ejot"></div>
                        <div class="company company_nseven"></div>
                    </div>
                </div>
            </section>
            <section id="endZone"></section>
        </div>

        <section id="footer">
            <div class="wrapper">
              <br>
                <li>All rights reserved by <?php echo $firstname; ?> <?php echo $lastname; ?></li>
                <li>DennisRauscher.de © <?php echo $firstname; ?> <?php echo $lastname; ?> 2016 - 2018</li>
                <br>
                <li class="impressum"><b><?php echo $txt_imprint; ?></b></li>
                <div class="seperator"/>
                <br/>
                <table>
                    <tr><td><b><?php echo $txt_owner; ?>:</b></td><td></td></tr>
                    <tr><td><?php echo $firstname; ?> <?php echo $lastname; ?></td><td></td></tr>
                </table>
                <br/>
                <table>
                    <tr><td><b><?php echo $txt_contact_me; ?>:</b></td><td></td></tr>
                    <tr><td><?php echo $txt_phone; ?> :</td><td><?php echo $phone; ?></td></tr>
                    <tr><td>E-Mail :</td><td><?php echo $email; ?></td></tr>
                </table>
                <br/>
                <table>
                    <tr><td><b><?php echo $txt_adress; ?>:</b></td><td></td></tr>
                    <tr><td><?php echo $street; ?></td><td><?php echo $location; ?></td></tr>
                </table>
            </div>
        </section>
    </body>
</html>
